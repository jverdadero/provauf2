<?php
/**
 * Created by PhpStorm.
 * User: jovan
 * Date: 3/7/2019
 * Time: 4:56 PM
 */

require_once ("../controller/finca_controller.php");
?>

<form name="update" id="update" action="../controller/add_finca.php" method="post">
    <label for="prop">Propietari</label>
    <input type="text" name="prop" id="prop" required>
    <br>
    <label for="posicio">Posicio</label>
    <input type="text" name="posicio" id="posicio">
    <SELECT id="ad" name="prop_id">
        <?php
        $list_fincas=get_propietari();
        foreach ($list_fincas as $propietari) {
            ?>
            <option value="<?=$propietari->getId()?>">
                <?=$propietari->getPropietari()?>,<?=$propietari->getPosicio()?>
            </option>
            <?php
        }
        ?>
    </SELECT>
    <br>
    <input type="submit" value="Actualitzar">
</form>
