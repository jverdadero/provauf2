<?php
/**
 * Created by PhpStorm.
 * User: jovan
 * Date: 3/7/2019
 * Time: 4:18 PM
 */

require_once ("../model/Finca.php");
require_once ("../model/FincaDAO.php");

$prop=$_POST["prop"];
$posicio=$_POST["posicio"];

$propietari = new Finca();
$propietari->setPropietari($prop);
$propietari->setPosicio($posicio);

save_propietari($propietari);

include ("../view/succesful.html");


function save_propietari($propietari)
{

    $fincaDAO = new FincaDAO();

    return $fincaDAO->save_propietari($propietari);
}
?>