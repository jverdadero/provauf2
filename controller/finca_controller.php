<?php
/**
 * Created by PhpStorm.
 * User: jovan
 * Date: 3/7/2019
 * Time: 4:47 PM
 */
require_once ("../model/Finca.php");
require_once ("../model/FincaDAO.php");

function get_propietari(){
    $fincaDAO = new FincaDAO();

    $finca_list=$fincaDAO->get_propietari();

    return $finca_list;
}

?>