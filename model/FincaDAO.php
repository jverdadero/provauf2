<?php
/**
 * Created by PhpStorm.
 * User: jovan
 * Date: 3/7/2019
 * Time: 3:50 PM
 */
require_once ("DBConfig.php");
require_once ("Finca.php");

class FincaDAO {
    function get_propietari() {
        try {
            $dbh = $this->get_connection();

            $stmt = $dbh->prepare("SELECT * FROM fincas");
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'Finca');

            $stmt->execute();

            $propietari_list = $stmt->fetchAll();


            return $propietari_list;


        } catch (PDOException $e) {
            print_r($stmt->errorInfo());
            echo $e->getMessage();
        }
    }

    function save_propietari($propietari) {
        try {
            $dbh = $this->get_connection();

            $stmt = $dbh->prepare("INSERT INTO fincas (propietari,posicio) VALUES (?,?)");
            $stmt->bindParam(1, $propietari->getPropietari());
            $stmt->bindParam(2, $propietari->getPosicio());

            $stmt->execute();

            $propietari->setId($dbh->lastInsertId());

        } catch (PDOException $e) {
            print_r($stmt->errorInfo());
            echo $e->getMessage();
        }
    }

    function delete_finca($propietari){
        try {
            $dbh = $this->get_connection();

            $stmt = $dbh->exec("DELETE FROM fincas (propietari,posicio) VALUES (?,?)");

            $stmt->bindParam(1, $propietari->getPropietari());
            $stmt->bindParam(2, $propietari->getPosicio());

            $stmt->execute();

            $propietari->setId($dbh->lastInsertId());

        } catch (PDOException $e) {
            print_r($stmt->errorInfo());
            echo $e->getMessage();
        }
    }

    private function get_connection()
    {
        $dbConfig = new DBConfig();
        $dsn = "mysql:host=" . $dbConfig->DB_HOST .
            ";dbname=" . $dbConfig->DB_NAME;

        try {
            $dbh = new PDO($dsn, $dbConfig->DB_USER, $dbConfig->DB_PASSWORD);
            return $dbh;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function getId($prop,$posicio)
    {
        try{
            $dbh = $this->get_connection();


            $stmt = $dbh->prepare("SELECT id FROM fincas WHERE propietari=? AND posicio=?");

            $stmt->bindParam(1, $prop);
            $stmt->bindParam(2, $posicio);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            $stmt->execute();

            $propietari = $stmt->fetch();

            return $propietari['id'];


        } catch (PDOException $e)
        {
            print_r($stmt->errorInfo());
            echo $e->getMessage();
        }

    }
}
?>