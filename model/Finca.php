<?php
/**
 * Created by PhpStorm.
 * User: jovan
 * Date: 3/7/2019
 * Time: 3:48 PM
 */

class Finca
{
private $id;
private $propietari;
private $posicio;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPropietari()
    {
        return $this->propietari;
    }

    /**
     * @param mixed $propietari
     */
    public function setPropietari($propietari)
    {
        $this->propietari = $propietari;
    }

    /**
     * @return mixed
     */
    public function getPosicio()
    {
        return $this->posicio;
    }

    /**
     * @param mixed $posicio
     */
    public function setPosicio($posicio)
    {
        $this->posicio = $posicio;
    }
}